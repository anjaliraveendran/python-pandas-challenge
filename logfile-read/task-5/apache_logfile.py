import requests
import json
import xlsxwriter 

# SLACK_URL = "https://hooks.slack.com/services/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"
FILENAME ="Apache_2k.log"

# workbook = xlsxwriter.Workbook('hello.xlsx') 
  
# worksheet = workbook.add_worksheet() 

# worksheet.write('A1', 'Hello..') 
# worksheet.write('B1', 'Geeks') 
# worksheet.write('C1', 'For') 
# worksheet.write('D1', 'Geeks') 
  
# workbook.close()

def read_logs():
    with open(FILENAME, 'r') as f:
        log_lines = f.readlines() #f.read reads all the data from it # & gives every line as a new string # in an array
                                  
    return log_lines

def parse_line(line):
    if line.lower().find('error') > 0:
        return True
    return False

def main():
    # print('main is running!')
    log_lines = read_logs()

    workbook = xlsxwriter.Workbook('errors.xlsx') 
    worksheet = workbook.add_worksheet() 

    for line in log_lines:
        if parse_line(line) == True:            
            print('error line: ')
            print(line)
            worksheet.write(line) 
        workbook.close()
    

if __name__ == '__main__':
    main()